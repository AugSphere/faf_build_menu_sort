local UIUtil = import('/lua/ui/uiutil.lua')
local DiskGetFileInfo = UIUtil.DiskGetFileInfo
local LayoutHelpers = import('/lua/maui/layouthelpers.lua')
local Group = import('/lua/maui/group.lua').Group
local Bitmap = import('/lua/maui/bitmap.lua').Bitmap
local SpecialGrid = import('/lua/ui/controls/specialgrid.lua').SpecialGrid
local Checkbox = import('/lua/maui/checkbox.lua').Checkbox
local Button = import('/lua/maui/button.lua').Button
local FixableButton = import('/lua/maui/button.lua').FixableButton
local Edit = import('/lua/maui/edit.lua').Edit
local StatusBar = import('/lua/maui/statusbar.lua').StatusBar
local GameCommon = import('/lua/ui/game/gamecommon.lua')
local GameMain = import('/lua/ui/game/gamemain.lua')
local RadioGroup = import('/lua/maui/mauiutil.lua').RadioGroup
local Tooltip = import('/lua/ui/game/tooltip.lua')
local TooltipInfo = import('/lua/ui/help/tooltips.lua').Tooltips
local Prefs = import('/lua/user/prefs.lua')
local EnhanceCommon = import('/lua/enhancementcommon.lua')
local Templates = import('/lua/ui/game/build_templates.lua')
local BuildMode = import('/lua/ui/game/buildmode.lua')
local UnitViewDetail = import('/lua/ui/game/unitviewDetail.lua')
local options = Prefs.GetFromCurrentProfile('options')
local Effect = import('/lua/maui/effecthelpers.lua')
local TemplatesFactory = import('/lua/ui/templates_factory.lua')
local straticonsfile = import('/lua/ui/game/straticons.lua')
local Select = import('/lua/ui/game/selection.lua')
local Factions = import('/lua/factions.lua').Factions
local FactionInUnitBpToKey = import('/lua/factions.lua').FactionInUnitBpToKey

local prevBuildables = false
local prevSelection = false
local prevBuildCategories = false

-- Flag to indicate if every selected unit is a factory
local allFactories = nil
if options.gui_templates_factory ~= 0 then
    allFactories = false
end

local missingIcons = {}
local dragging = false
local index = nil -- Index of the item in the queue currently being dragged
local originalIndex = false -- Original index of selected item (so that UpdateBuildQueue knows where to modify it from)
local oldQueue = {}
local modifiedQueue = {}
local updateQueue = true -- If false then queue won't update in the ui
local modified = false -- If false then buttonrelease will increase buildcount in queue
local dragLock = false -- To disable quick successive drags, which doubles the units in the queue

-- locals for Keybind labels in build queue
local hotkeyLabel_addLabel = import('/lua/keymap/hotkeylabelsUI.lua').addLabel
local idRelations = {}
local upgradeKey = false
local upgradesTo = false
local allowOthers = true

local userIconConfigById = import('/mods/BuildMenuSort/sortTableGen.lua').userIconConfigById

local function SortFunc(unit1, unit2)
    local bp1 = __blueprints[unit1]
    local bp2 = __blueprints[unit2]
    local v1 = userIconConfigById[unit1].BuildIconSortPriority or bp1.BuildIconSortPriority or bp1.StrategicIconSortPriority
    local v2 = userIconConfigById[unit2].BuildIconSortPriority or bp2.BuildIconSortPriority or bp2.StrategicIconSortPriority

    if v1 >= v2 then
        return false
    else
        return true
    end
end

function FormatData(unitData, type)
    local retData = {}
    if type == 'construction' then
        local sortedUnits = {}
        local sortCategories = {
            categories.SORTCONSTRUCTION,
            categories.SORTECONOMY,
            categories.SORTDEFENSE,
            categories.SORTSTRATEGIC,
            categories.SORTINTEL,
            categories.SORTOTHER,
        }
        local miscCats = categories.ALLUNITS
        local borders = {}
        for i, v in sortCategories do
            local category = v
            local index = i - 1
            local tempIndex = i
            while index > 0 do
                category = category - sortCategories[index]
                index = index - 1
            end
            local units = EntityCategoryFilterDown(category, unitData)
            table.insert(sortedUnits, units)
            miscCats = miscCats - v
        end

        table.insert(sortedUnits, EntityCategoryFilterDown(miscCats, unitData))

        -- Get function for checking for restricted units
        local IsRestricted = import('/lua/game.lua').IsRestricted

        -- This section adds the arrows in for a build icon which is an upgrade from the
        -- selected unit. If there is an upgrade chain, it will display them split by arrows.
        -- I'm excluding Factories from this for now, since the chain of T1 -> T2 HQ -> T3 HQ
        -- or T1 -> T2 Support -> T3 Support is not supported yet by the code which actually
        -- looks up, stores, and executes the upgrade chain. This needs doing for 3654.
        local unitSelected = sortedOptions.selection[1]
        local isStructure = EntityCategoryContains(categories.STRUCTURE - categories.FACTORY, unitSelected)

        for i, units in sortedUnits do
            table.sort(units, SortFunc)
            local index = i
            if table.getn(units) > 0 then
                if table.getn(retData) > 0 then
                    table.insert(retData, {type = 'spacer'})
                end

                for index, unit in units do
                    -- Show UI data/icons only for not restricted units
                    local restrict = false
                    if not IsRestricted(unit, GetFocusArmy()) then
                        local bp = __blueprints[unit]
                        -- Check if upgradeable structure
                        if isStructure and
                                bp and bp.General and
                                bp.General.UpgradesFrom and
                                bp.General.UpgradesFrom ~= 'none' then

                            restrict = IsRestricted(bp.General.UpgradesFrom, GetFocusArmy())
                            if not restrict then
                                table.insert(retData, {type = 'arrow'})
                            end
                        end

                        if not restrict then
                            table.insert(retData, {type = 'item', id = unit})
                        end
                    end
                end
            end
        end

        CreateExtraControls('construction')
        SetSecondaryDisplay('buildQueue')
    elseif type == 'selection' then
        local sortedUnits = {
            [1] = {cat = "ALLUNITS", units = {}},
            [2] = {cat = "LAND", units = {}},
            [3] = {cat = "AIR", units = {}},
            [4] = {cat = "NAVAL", units = {}},
            [5] = {cat = "STRUCTURE", units = {}},
            [6] = {cat = "SORTCONSTRUCTION", units = {}},
        }

        local lowFuelUnits = {}
        local idleConsUnits = {}

        for _, unit in unitData do
            local id = unit:GetBlueprint().BlueprintId

            if unit:IsInCategory('AIR') and unit:GetFuelRatio() < .2 and unit:GetFuelRatio() > -1 then
                if not lowFuelUnits[id] then
                    lowFuelUnits[id] = {}
                end
                table.insert(lowFuelUnits[id], unit)
            elseif options.gui_seperate_idle_builders ~= 0 and unit:IsInCategory('CONSTRUCTION') and unit:IsIdle() then
                if not idleConsUnits[id] then
                    idleConsUnits[id] = {}
                end
                table.insert(idleConsUnits[id], unit)
            else
                local cat = 0
                for i, t in sortedUnits do
                    if unit:IsInCategory(t.cat) then
                        cat = i
                    end
                end

                if not sortedUnits[cat].units[id] then
                    sortedUnits[cat].units[id] = {}
                end

                table.insert(sortedUnits[cat].units[id], unit)
            end
        end

        local function insertSpacer(didPutUnits)
            if didPutUnits then
                table.insert(retData, {type = 'spacer'})
                return not didPutUnits
            end
        end

        -- Sort selected units into order and insert spaces
        local didPutUnits = false
        for _, t in sortedUnits do
            didPutUnits = insertSpacer(didPutUnits)

            retData, didPutUnits = insertIntoTableLowestTechFirst(t.units, retData, false, false)
        end

        -- Split out low fuel
        didPutUnits = insertSpacer(didPutUnits)
        retData, didPutUnits = insertIntoTableLowestTechFirst(lowFuelUnits, retData, true, false)

        -- Split out idle constructors
        didPutUnits = insertSpacer(didPutUnits)
        retData, didPutUnits = insertIntoTableLowestTechFirst(idleConsUnits, retData, false, true)

        -- Remove trailing spacer if there is one
        if retData[table.getn(retData)].type == 'spacer' then
            table.remove(retData, table.getn(retData))
        end

        CreateExtraControls('selection')
        SetSecondaryDisplay('attached')

        import(UIUtil.GetLayoutFilename('construction')).OnTabChangeLayout(type)
    elseif type == 'templates' then
        table.sort(unitData, function(a, b)
            if a.key and not b.key then
                return true
            elseif b.key and not a.key then
                return false
            elseif a.key and b.key then
                return a.key <= b.key
            elseif a.name == b.name then
                return false
            else
                if LOC(a.name) <= LOC(b.name) then
                    return true
                else
                    return false
                end
            end
        end)
        for _, v in unitData do
            table.insert(retData, {type = 'templates', id = 'template', template = v})
        end
        CreateExtraControls('templates')
        SetSecondaryDisplay('buildQueue')
    else
        -- Enhancements
        local existingEnhancements = EnhanceCommon.GetEnhancements(sortedOptions.selection[1]:GetEntityId())
        local slotToIconName = {
            RCH = 'ra',
            LCH = 'la',
            Back = 'b',
        }
        local filteredEnh = {}
        local usedEnhancements = {}
        local restEnh = EnhanceCommon.GetRestricted()

        -- Filter enhancements based on restrictions
        for index, enhTable in unitData do
            if not restEnh[enhTable.ID] and not string.find(enhTable.ID, 'Remove') then
                table.insert(filteredEnh, enhTable)
            end
        end

        local function GetEnhByID(id)
            for i, enh in filteredEnh do
                if enh.ID == id then
                    return enh
                end
            end
        end

        local function FindDependancy(id)
            for i, enh in filteredEnh do
                if enh.Prerequisite and enh.Prerequisite == id then
                    return enh.ID
                end
            end
        end

        local function AddEnhancement(enhTable, disabled)
            local iconData = {
                type = 'enhancement',
                enhTable = enhTable,
                unitID = enhTable.UnitID,
                id = enhTable.ID,
                icon = enhTable.Icon,
                Selected = false,
                Disabled = disabled,
            }
            if existingEnhancements[enhTable.Slot] == enhTable.ID then
                iconData.Selected = true
            end
            table.insert(retData, iconData)
        end

        for i, enhTable in filteredEnh do
            if not usedEnhancements[enhTable.ID] and not enhTable.Prerequisite then
                AddEnhancement(enhTable, false)
                usedEnhancements[enhTable.ID] = true
                if FindDependancy(enhTable.ID) then
                    local searching = true
                    local curID = enhTable.ID
                    while searching do
                        table.insert(retData, {type = 'arrow'})
                        local tempEnh = GetEnhByID(FindDependancy(curID))
                        local disabled = true
                        if existingEnhancements[enhTable.Slot] == tempEnh.Prerequisite then
                            disabled = false
                        end
                        AddEnhancement(tempEnh, disabled)
                        usedEnhancements[tempEnh.ID] = true
                        if FindDependancy(tempEnh.ID) then
                            curID = tempEnh.ID
                        else
                            searching = false
                            if table.getsize(usedEnhancements) <= table.getsize(filteredEnh) - 1 then
                                table.insert(retData, {type = 'spacer'})
                            end
                        end
                    end
                else
                    if table.getsize(usedEnhancements) <= table.getsize(filteredEnh) - 1 then
                        table.insert(retData, {type = 'spacer'})
                    end
                end
            end
        end
        CreateExtraControls('enhancement')
        SetSecondaryDisplay('buildQueue')
    end
    import(UIUtil.GetLayoutFilename('construction')).OnTabChangeLayout(type)


    if type == 'templates' and allFactories then
        -- Replace Infinite queue with Create template
        Tooltip.AddCheckboxTooltip(controls.extraBtn1, 'save_template')
        if table.getsize(currentCommandQueue) > 0 then
            controls.extraBtn1:Enable()
            controls.extraBtn1.OnClick = function(self, modifiers)
                TemplatesFactory.CreateBuildTemplate(currentCommandQueue)
            end
        else
            controls.extraBtn1:Disable()
        end
        controls.extraBtn1.icon.OnTexture = UIUtil.UIFile('/game/construct-sm_btn/template_on.dds')
        controls.extraBtn1.icon.OffTexture = UIUtil.UIFile('/game/construct-sm_btn/template_off.dds')
        if controls.extraBtn1:IsDisabled() then
            controls.extraBtn1.icon:SetTexture(controls.extraBtn1.icon.OffTexture)
        else
            controls.extraBtn1.icon:SetTexture(controls.extraBtn1.icon.OnTexture)
        end
    end

    return retData
end
