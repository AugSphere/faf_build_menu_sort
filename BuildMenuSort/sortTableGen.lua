local Prefs = import('/lua/user/prefs.lua')
local Game = import('/lua/game.lua')

local buildable_in_factories_ids = EntityCategoryGetUnitList(categories.BUILTBYTIER3FACTORY + categories.TRANSPORTBUILTBYTIER3FACTORY)

local function create_unit_info_table(cats)
    local filtered_table = {}
    local filtered_ids = EntityCategoryFilterDown(cats, buildable_in_factories_ids)
    for _, unit_id in filtered_ids do
        filtered_table[unit_id] = {}
    end
    return filtered_table
end

local types = {
    ["land"]  = categories.LAND,
    ["air"]   = categories.AIR,
    ["naval"] = categories.NAVAL
}

local factions = {
    ["aeon"]     = categories.AEON,
    ["cybran"]   = categories.CYBRAN,
    ["uef"]      = categories.UEF,
    ["seraphim"] = categories.SERAPHIM
}

local tech ={
    ["tech1"] = categories.TECH1,
    ["tech2"] = categories.TECH2,
    ["tech3"] = categories.TECH3
}

local function create_default_config_table()
    local icon_config_table = {}
    local unit_table = {}
    for unit_faction, faction_cat in factions do
        icon_config_table[unit_faction] = {}
        for unit_type, type_cat in types do
            icon_config_table[unit_faction][unit_type] = {}
            for unit_tier, tech_cat in tech do
                icon_config_table[unit_faction][unit_type][unit_tier] = {}
                unit_table = icon_config_table[unit_faction][unit_type][unit_tier]
                local filtered_ids = EntityCategoryFilterDown(faction_cat * type_cat * tech_cat, buildable_in_factories_ids)
                for _, unit_id in filtered_ids do
                    unit_table[unit_id] = {
                        ["BuildIconSortPriority"] = __blueprints[unit_id].BuildIconSortPriority or 0,
                        ["Description"]           = __blueprints[unit_id].Description,
                        ["UnitName"]              = __blueprints[unit_id].General.UnitName,
                    }
                end
            end
        end
    end
    return icon_config_table
end

local userIconConfigTable = Prefs.GetFromCurrentProfile("BuildIconSortTable")
if userIconConfigTable == nil then
    userIconConfigTable = create_default_config_table()
    Prefs.SetToCurrentProfile("BuildIconSortTable", userIconConfigTable)
end

userIconConfigById = create_unit_info_table(categories.SELECTABLE)
for unit_faction, faction_cat in factions do
    for unit_type, type_cat in types do
        for unit_tier, tech_cat in tech do
            unit_table = userIconConfigTable[unit_faction][unit_type][unit_tier]
            for unit_id in unit_table do
                userIconConfigById[unit_id].BuildIconSortPriority = unit_table[unit_id]["BuildIconSortPriority"]
            end
        end
    end
end

